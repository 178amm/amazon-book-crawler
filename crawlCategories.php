<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php     

    //Include from app
    include_once("app/AmazonLinkCrawler.php");
    include_once("app/AmazonBookCrawler.php");
    include_once("app/model/AmazonLinksModel.php");
    include_once("app/model/AmazonBooksModel.php");

    //Function declaration
    function start()
    {
        //Crawls the first 3 links from each category
        $maxPages = 3;
        //Accion y aventura
        $categoryURL0 = 'http://www.amazon.es/s/ref=lp_599364031_nr_n_0?rh=n%3A599364031%2Cn%3A%21599365031%2Cn%3A902675031&bbn=599365031&ie=UTF8&qid=1414866285&rnid=599365031';
        $categoryID0 = 0;
        //Arte, cine y fotografia
        $categoryURL1 = 'http://www.amazon.es/s/ref=lp_599364031_nr_n_1?rh=n%3A599364031%2Cn%3A%21599365031%2Cn%3A902486031&bbn=599365031&ie=UTF8&qid=1414866285&rnid=599365031';
        $categoryID1 = 1;
        //Biografias, diarios y hechos reales
        $categoryURL2 = 'http://www.amazon.es/s/ref=lp_599364031_nr_n_2?rh=n%3A599364031%2Cn%3A%21599365031%2Cn%3A902498031&bbn=599365031&ie=UTF8&qid=1414866285&rnid=599365031';
        $categoryID2 = 2;

        //Start crawling
        $linksCrawler = new AmazonLinkCrawler();
        $linksCrawler->startCrawling($categoryURL0, $categoryID0, $maxPages);
        $linksCrawler->startCrawling($categoryURL1, $categoryID1, $maxPages);
        $linksCrawler->startCrawling($categoryURL2, $categoryID2, $maxPages);
    }

    function resume()
    {
        //Continues crawling 3 links from each category since the last crawling
        $maxPages = 3;
        //Accion y aventura
        $categoryID0 = 0;
        //Arte, cine y fotografia
        $categoryID1 = 1;
        //Biografias, diarios y hechos reales
        $categoryID2 = 2;

        //Start crawling
        $linksCrawler = new AmazonLinkCrawler();
        $linksCrawler->resumeCrawling($categoryID0, $maxPages);
        $linksCrawler->resumeCrawling($categoryID1, $maxPages);
        $linksCrawler->resumeCrawling($categoryID2, $maxPages);
    }

?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1> Category crawler page </h1>

        <a href="?run=start">Start from the beggining</a>
    </br>
        <a href="?run=resume">Resume</a>
    </br>
        <a href="./index.html">Back..</a>
    </br>

        <?php
        if (isset($_GET['run']))
        {
            echo ("<p> Crawling, it may take a while... </>");
            switch ($_GET['run']) {
                case 'start':
                    start();
                    break;
                case 'resume':
                    resume();
                    break;
                default:
                    echo ("eror");
                    break;
            }
        }
        ?>
    </br>
    </body>
</hmtl>