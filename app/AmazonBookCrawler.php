<?php

//Include phpQuery 0.9
include_once("libs/phpQuery/phpQuery.php");
//Include from amazonbookcrawler
include_once("app/model/AmazonBooksModel.php");

class AmazonBookCrawler
{
	/**
	 * @var array
	 */
	private $config;
	/**
	 * @var string
	 */
	private $ljump = "<br />";

	/**
	 * Constructor. Just loads the config.ini file and 
	 *  set the host.
	 * @return void
	 */
	function __construct()
	{
		//Load the configuration file 
		$this->config = parse_ini_file("/config.ini");

		//Set the host and link to analyze
		phpQuery::ajaxAllowHost($this->config['amazon_host']);
	}

	/**
	 * Resumes crawling since the last execution. If the crawler
	 * 	never has been run, starts from the first link.
	 * @param uint $categoryID 
	 * @param uint $maxBooks
	 * @return void
	 */
	public function resumeCrawling($categoryID, $maxBooks)
	{
		//Inits the model
		$linksModel = new AmazonLinksModel();
		$linksModel->connect();

		//Inits the book model
		$booksModel = new AmazonBooksModel();
		$booksModel->connect();

		//For time measuring
		$itime = microtime(TRUE);

		//Gets the last status row for knowing where to start from.
		// If never crawled before, starts from the fist link (0);
		$startFrom = $booksModel->findLastStatus($categoryID)['lastCrawledBookID'];
		if ($startFrom == NULL)
			$startFrom = 0;

		//Creates new status row
		$currentStatus = $booksModel->newStatus();
		$currentStatus->categoryID = $categoryID;

		try
		{
			//Gets the batch
			$batch = $linksModel->findBookLinkBatch($categoryID, $startFrom, $startFrom+$maxBooks);
		}
		catch (Exception $e)
		{
			//An error happens, set the error flag in the status
			// and update the number of crawled pages
			echo ("Unable to retrieve the booksBatch <br/>");
		}

		//Closes database connection
		//$linksModel->close();

		try
		{
			//Retrieves the bookData from the links batch
			$books = $this->crawlBookBatch($batch, $categoryID);

			//If things were ok, update currentStatus values
			$currentStatus->crawledPages = $maxBooks;
			$currentStatus->processingTime = (microtime(TRUE)-$currentStatus->processingTime);
		}
		catch (Exception $e)
		{
			//An error happens, set the error flag in the status
			// and update the number of crawled pages
			echo ("Unable to retrieve the bookData batch <br/>");
			$currentStatus->error = true;
		}

		//Stores all book data in a batch
		$booksModel->newBookBatch($books, $categoryID);

		//Update the linkStatus row
		$currentStatus->lastCrawledBookID = $booksModel->findLastBook()['id'];
		$booksModel->update($currentStatus);

		//Closes database connection
		$linksModel->close();
	}

	/**
	 * Crawls an entire book batch from the given links and 
	 *  category.
	 * @param array $batchLinks 
	 * @param uint $categoryID 
	 * @return array
	 */
	public function crawlBookBatch($batchLinks, $categoryID)
	{
		//Inits. book array
		$books = array();

		foreach ($batchLinks as $bookLink)
		{
			//Crawls book and adds it to the array
			$book = $this->crawlBookPage($bookLink['link'], $bookLink['id'], $categoryID);
			array_push($books, $book);
		}

		//Return the array of books (data)
		return ($books);
	}

	/**
	 * Crawls a single given book page, returns a $bookdata object 
	 *  with properties defined
	 * @param string $link 
	 * @param uint $linkID 
	 * @param uint $categoryID 
	 * @return phpRedBeanObject
	 */
	public function crawlBookPage($link, $linkID, $categoryID)
	{
		//Loading the html body of the page
		$doc = phpQuery::newDocumentHTML('</html>');
		pq($doc)->load($link . " body");

		//Get book title
		$title = pq('> span', $doc->find('#btAsinTitle'));
		pq('> span', $title)->remove(); //remove kindle advert
		$book['title'] = $title->text();
		
		//Get book price
		$price = $doc->find('.priceLarge');
		$book['price'] = $price->text();

		//Get book rating
		$rating = $doc->find('.gry.txtnormal.acrRating');
		$book['rating'] = $rating->text();

		//Get author **************NI IDEA DE COMO SACARLO
		$book['authorID'] = "authoriD";
		//

		//Get editorial **************NI IDEA DE COMO SACARLO
		//$book['editorial'] = "editorial";
		//

		//Extract the rest of the data
		$extra = $doc->find('#detail_bullets_id')->find('ul');

			//Get ISBN (ASIN for kindle books)
			$book['isbn'] = pq(':eq(12)', $extra)->text();

			//Get number of pages
			$book['nPages'] = pq(':eq(4)', $extra)->text();

		//Adds extra values passed by the function
			$book['categoryID'] = $categoryID;
			$book['linkID'] = $linkID;

		//Return book's data
		return ($book);
	}
}

?>