<?php

//Include phpQuery 0.9
include_once("libs/phpQuery/phpQuery.php");
//Require RedBeanPHP
require_once("libs/RedBeanPHP/rb.php");
//Include from amazonbookcrawler

class AmazonLinksModel
{
	//Variables def.
	private $config;
	const BOOKLINKCRAWLERSTATUS_TABLENAME = 'booklinkcrawlerstatus';
	const BOOKLINKS_TABLENAME = 'booklinks';
	const CATEGORYLINKS_TABLENAME = 'categorylinks';

	//------CONSTRUCTOR
	function __construct()
	{
		//Load the configuration file 
		$this->config = parse_ini_file("/config.ini");
	}

	/**
	 * Connects to the database. Database connection
	 * parameters are extracted from the config.ini file
	 * @return void
	 */
	function connect()
	{
		$connectstr = 'mysql:host=' . $this->config['db_host'] . ';';
		$connectstr .= 'dbname=' . $this->config['db_name'];

		try
		{
			R::setup($connectstr,
			$this->config['db_user'],
			$this->config['db_pass']);
		}
		catch(Exception $e)
		{
			echo("Already connected to the database </br>");
		}

	}

	/**
	 * Closes database connection
	 * @return void
	 */
	function close()
	{
		R::close();
	}

	/**
	 * Updates the object data passed as parameter 
	 * in the database
	 * @param mixed $redBeanPHPObject 
	 * @return void
	 */
	function update($redBeanPHPObject)
	{
		R::store($redBeanPHPObject);
	}

	/**
	 * Creates a bookCrawlerStatus row with default values
	 * @return redBeanPHPObject
	 */
	function newStatus()
	{
		$linkstatus = R::dispense(self::BOOKLINKCRAWLERSTATUS_TABLENAME);

		$linkstatus->processingTime = microtime(TRUE);
		$linkstatus->crawledPages = 0;
		$linkstatus->categoryID = 3;
		$linkstatus->error = false;
		$linkstatus->crawlingDate = R::isoDateTime();

		$id = R::store($linkstatus);

		return ($linkstatus);
	}

	/**
	 * Creates a new BookLink from the given values
	 * @param uint $nameID 
	 * @param uing $categoryID 
	 * @param string $link 
	 * @return void
	 */
	public function newBookLink($nameID, $categoryID, $link)
	{
		$bookLink = R::dispense(self::BOOKLINKS_TABLENAME);
		$bookLink->categoryID = $categoryID;
		$bookLink->nameID = $nameID; //UNIQUE
		$bookLink->link = $link;

		try
		{
			$id = R::store($bookLink);
		} 
		catch (Exception $e) 
		{
			echo ("Book Link already exists <br/>");
		}

	}

	/**
	 * Creates a new category Link from the given values
	 * @param uint $categoryID 
	 * @param string $link 
	 * @param uint $pageNumber 
	 * @return 
	 */
	public function newCategoryLink($categoryID, $link, $pageNumber)
	{
		$categoryLink = R::dispense(self::CATEGORYLINKS_TABLENAME);
		$categoryLink->uid = strval($categoryID) . strval($pageNumber); //UNIQUE
		$categoryLink->categoryID = $categoryID;
		$categoryLink->pageNumber = $pageNumber;
		$categoryLink->link = $link;

		try
		{
			$id = R::store($categoryLink);
		} 
		catch (Exception $e) 
		{
			echo ("Category Link already exists <br/>");
		}
	}

	/**
	 * Creates a new bookLink batch rom bookLinks values and
	 *  a given gategory ID
	 * @param redBeanPHPObject $bookLinks 
	 * @param uint $categoryID 
	 * @return void
	 */
	public function newBookLinkBatch($bookLinks, $categoryID)
	{
		foreach ($bookLinks as $bLink)
		{
			$this->newBookLink($bLink['bookTitle'], $categoryID, $bLink['bookLink']);
		}
	}

	/**
	 * Creates a new categoryLink batch from links and a given 
	 *  categoryID
	 * @param redBeanPHPObject $categoryLinks 
	 * @param uint $categoryID 
	 * @return void
	 */
	public function newCategoryLinkBatch($categoryLinks, $categoryID)
	{

		foreach ($categoryLinks as $catLink)
		{
			//Falta el numero de pagina
			$this->newCategoryLink($categoryID, $catLink['link'], $catLink['pageNumber']);
		}
	}

	/**
	 * Access to the database and retrieves
	 *  the last link bookLinkCrawler status from a given category
	 * @param uint $categoryID 
	 * @return void
	 */
	public function findLastStatus($categoryID)
	{
		//Query
		$linkstatus = R::findOne(self::BOOKLINKCRAWLERSTATUS_TABLENAME,
			' category_id = ? ORDER BY crawling_date DESC ' , [$categoryID]);

		return ($linkstatus);
	}

	/**
	 * Given a categoryID and a pageNumber,
	 *  returns the categoryLink object from the database
	 * @param uint $categoryID 
	 * @param uint $pageNumber 
	 * @return redBeanPHPObject
	 */
	public function findCategoryLink($categoryID, $pageNumber)
	{
		//Query
		$categoryLink = R::findOne(self::CATEGORYLINKS_TABLENAME,
			' category_id = ? AND page_number = ? ' , [$categoryID, $pageNumber]);

		return ($categoryLink);
	}

	/**
	 * Finds in the database the last inserted categoryLink
	 *  by its iD
	 * @param uint $categoryID 
	 * @return redBeanPHPObject
	 */
	public function findLastCategoryLink($categoryID)
	{
		//Query
		$categoryLink = R::findOne(self::CATEGORYLINKS_TABLENAME,
			' category_id = ? ORDER BY page_number DESC' , [$categoryID]);

		return ($categoryLink);
	}

	/**
	 * Find a bookLink batch by a given number range
	 * @param uint $categoryID 
	 * @param uint $first 
	 * @param uint $last 
	 * @return array
	 */
	public function findBookLinkBatch($categoryID, $first, $last)
	{
		//Query
		$bookLinkBatch = R::find(self::BOOKLINKS_TABLENAME,
			' category_id = ? AND id > ? AND id <= ?' , [$categoryID, $first, $last]);

		return ($bookLinkBatch);
	}

}

?>