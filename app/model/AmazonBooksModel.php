<?php

//Include phpQuery 0.9
include_once("libs/phpQuery/phpQuery.php");
//Require RedBeanPHP
require_once("libs/RedBeanPHP/rb.php");
//Include from amazonbookcrawler

class AmazonBooksModel
{
	//Variables def.
	private $config;
	const BOOKCRAWLERSTATUS_TABLENAME = 'bookcrawlerstatus';
	const BOOKS_TABLENAME = 'books';

	/**
	 * Constructor, just reads the configuration data
	 * from the config.ini file 
	 * @return void
	 */
	function __construct()
	{
		//Load the configuration file 
		$this->config = parse_ini_file("/config.ini");
	}

	/**
	 * Connects to the database. Database connection
	 * parameters are extracted from the config.ini file
	 * @return void
	 */
	function connect()
	{
		$connectstr = 'mysql:host=' . $this->config['db_host'] . ';';
		$connectstr .= 'dbname=' . $this->config['db_name'];

		try
		{
			R::setup($connectstr,
			$this->config['db_user'],
			$this->config['db_pass']);
		}
		catch(Exception $e)
		{
			echo("Already connected to the database </br>");
		}

	}

	/**
	 * Closes database connection
	 * @return void
	 */
	function close()
	{
		R::close();
	}

	/**
	 * Updates the object data passed as parameter 
	 * in the database
	 * @param mixed $redBeanPHPObject 
	 * @return void
	 */
	function update($redBeanPHPObject)
	{
		R::store($redBeanPHPObject);
	}

	/**
	 * Creates a bookCrawlerStatus row with default values
	 * @return redBeanPHPObject
	 */
	public function newStatus()
	{
		$bookstatus = R::dispense(self::BOOKCRAWLERSTATUS_TABLENAME);

		$bookstatus->crawlingDate = R::isoDateTime();
		$bookstatus->processingTime = microtime(TRUE);
		$bookstatus->crawledBooks = 0;
		$bookstatus->categoryID = 0;
		$bookstatus->lastCrawledBookID = 0;

		$id = R::store($bookstatus);

		return ($bookstatus);
	}

	/**
	 * Insert a new book object in the database
	 *  from the given values
	 * @param uint $categoryID 
	 * @param string $title 
	 * @param string $price 
	 * @param string $rating 
	 * @param uint $authorID 
	 * @param uint $linkID 
	 * @param string $isbn 
	 * @param uint $nPages 
	 * @return redBeanpPHPObject
	 */
	public function newBook($categoryID, $title, $price, $rating, $authorID, $linkID, $isbn, $nPages)
	{
		$book = R::dispense(self::BOOKS_TABLENAME);

		$book->categoryID = $categoryID;
		$book->title = $title;
		$book->price = $price;
		$book->rating = $rating;
		$book->authorID = $authorID;
		$book->linkID = $linkID;
		//$book->editorial
		$book->isbn = $isbn;
		$book->nPages = $nPages;

		try 
		{
			$id = R::store($book);
		} 
		catch (Exception $e)
		{
			echo ("Book already exists <br/>");
		}

		return ($book);		
	}

	/**
	 * Insert a batch of books passed as an array with a 
	 *  given category
	 * @param array $books 
	 * @param uint $categoryID 
	 * @return void
	 */
	public function newBookBatch($books, $categoryID)
	{
		foreach ($books as $book)
		{
			$this->newBook($categoryID, $book['title'], $book['price'], $book['rating'], $book['authorID'], $book['linkID'], $book['isbn'], $book['nPages']);
		}
	}

	/**
	 * Find the last book inserted in the database
	 * @return redBeanpPHPObject
	 */
	public function findLastBook()
	{
		//Query
		$lastBook = R::findOne(self::BOOKS_TABLENAME,
			' ORDER BY id DESC ');

		return ($lastBook);		
	}

	/**
	 * Access to the database and retrieves 
	 *  the last link bookLinkCrawler status 
	 *  from a given category
	 * @param uint $categoryID 
	 * @return redBeanPHPObject
	 */
	public function findLastStatus($categoryID)
	{
		//Query
		$status = R::findOne(self::BOOKCRAWLERSTATUS_TABLENAME,
			' category_id = ? ORDER BY crawling_date DESC ' , [$categoryID]);

		return ($status);
	}
}