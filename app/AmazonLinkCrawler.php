<?php

//Include phpQuery 0.9
include_once("libs/phpQuery/phpQuery.php");
//Include from amazonbookcrawler
include_once("app/model/AmazonLinksModel.php");

class AmazonLinkCrawler
{
	/**
	 * @var array
	 */
	private $config;
	/**
	 * @var string
	 */
	private $ljump = "<br />";

	/**
	 * Constructor. Just loads the config.ini file and 
	 *  set the host.
	 * @return void
	 */
	function __construct()
	{
		//Load the configuration file 
		$this->config = parse_ini_file("/config.ini");

		//Set the host and link to analyze
		phpQuery::ajaxAllowHost($this->config['amazon_host']);
	}

	/**
	 * Init. a crawling proccess. Search for every bookLink
	 *  from a given category within the given range (maxPages)
	 * @param string $categoryURL 
	 * @param uint $categoryID 
	 * @param uint $maxPages 
	 * @return type
	 */
	public function startCrawling($categoryURL, $categoryID, $maxPages)
	{
		//Inits the model
		$linksModel = new AmazonLinksModel();
		$linksModel->connect();

		//For time measuring
		$itime = microtime(TRUE);

		//Creates new status row
		$currentStatus = $linksModel->newStatus();
		$currentStatus->categoryID = $categoryID;

		try
		{
			//Starts crawling, saves the batch
			$links = $this->crawlCategoryLinks($categoryURL, $maxPages);

			//If things were ok, update currentStatus values
			$currentStatus->crawledPages = $maxPages;
			$currentStatus->processingTime = (microtime(TRUE)-$currentStatus->processingTime);
		}
		catch (Exception $e)
		{
			//An error happens, set the error flag in the status
			// and update the number of crawled pages
			$currentStatus->error = true;
		}


		//Update the linkStatus row
		$linksModel->update($currentStatus);

		//Stores all book links in one batch
		$linksModel->newBookLinkBatch($links['bookLinks'], $categoryID);
		//Stores all category links in a batch
		$linksModel->newCategoryLinkBatch($links['categoryLinks'], $categoryID);


		//Closes database connection
		$linksModel->close();
	}

	/**
	 * Resumes crawling since the last execution
	 * @param uint $categoryID 
	 * @param uint $maxPages 
	 * @return type
	 */
	public function resumeCrawling($categoryID, $maxPages)
	{
		//Inits the model
		$linksModel = new AmazonLinksModel();
		$linksModel->connect();

		//For time measuring
		$itime = microtime(TRUE);

		//Creates new status row
		$currentStatus = $linksModel->newStatus();
		$currentStatus->categoryID = $categoryID;

		//Gets the last crawledPage link
		$lastPageLink = $linksModel->findLastCategoryLink($categoryID);		

		try
		{
			//Starts crawling, saves the batch
			$links = $this->crawlCategoryLinks($lastPageLink['link'], $maxPages);

			//If things were ok, update currentStatus values
			$currentStatus->crawledPages = $maxPages;
			$currentStatus->processingTime = (microtime(TRUE)-$currentStatus->processingTime);
		}
		catch (Exception $e)
		{
			//An error happens, set the error flag in the status
			// and update the number of crawled pages
			$currentStatus->error = true;
		}


		//Update the linkStatus row
		$linksModel->update($currentStatus);

		//Stores all book links in one batch
		$linksModel->newBookLinkBatch($links['bookLinks'], $categoryID);
		//Stores all category links in a batch
		$linksModel->newCategoryLinkBatch($links['categoryLinks'], $categoryID);


		//Closes database connection
		$linksModel->close();
	}

	/**
	 * Loop through each page of a category retrieving the links, 
	 *  return the book links from each category AND the links of 
	 *  the category itself.
	 * @param string $categoryURL 
	 * @param uint $maxPages 
	 * @return array
	 */
	public function crawlCategoryLinks($categoryURL, $maxPages)
	{
		//For time measuring
		//$itime = microtime(TRUE);

		//Declare array
		$categoryLinks = array();
		$categoryPageLinks = array();
		$bookLinks = array();

		//Loading the html body of the page
		$doc = phpQuery::newDocumentHTML('</html>');
		pq($doc)->load($categoryURL . " body");

		//Loops to each page retrieving category page links
		$currentpageurl = $categoryURL;
		$currentpagenumber = intval(pq('#pagn > .pagnCur', $doc)->text());

		for ($i=0; $i < $maxPages; $i++)
		{ 
			//Extract book links from a categorypage
			try 
			{
				//Saves the current categoryLink
				$catLink['link'] = $currentpageurl;
				$catLink['pageNumber'] = $currentpagenumber;
				array_push($categoryLinks, $catLink);

				//Extract bookLinks from the categoryPage
				$categoryPageLinks = $this->crawlCategoryPageLinks($currentpageurl, $i);

				//Update next's url page and adds it to the list
				$currentpageurl = $categoryPageLinks['nextPageLink'];
				$currentpagenumber = $categoryPageLinks['nextPageNumber'];

				//Append all the links obtained to the main array
				$bookLinks = array_merge($bookLinks, $categoryPageLinks['categoryPageLinks']);
			} 
			catch (Exception $e) 
			{
				$message = "Error Processing Links, Max Attempts reached (" . $this->config['max_connection_attempts'] . ")";
				$message .= " in page " . $i+1;
				echo($message);

				throw new Exception($message, 1);
			}
		}

		$links['bookLinks'] = $bookLinks;
		$links['categoryLinks'] = $categoryLinks; //Last item doesnt count

		//Return links
		return ($links);
	}

	/**
	 * Crawls a single page looking for book's links, 
	 *  returns them into an array $categorylinks object
	 * @param string $pageURL 
	 * @param uint $numPage 
	 * @return array
	 */
	public function crawlCategoryPageLinks($pageURL, $numPage=0)
	{

		//Set max connection attempts from the conf. file
		$maxConnectionAttempts = $this->config['max_connection_attempts'];

		//Declare array
		$categoryPageLinks = array();
		$categoryLinks = array();

		//Loading the html body of the page
		$doc = phpQuery::newDocumentHTML('</html>');
		pq($doc)->load($pageURL . " body");

		//Here tries to "extract" the data from the page through several methods, 
		// if one doesnt work, tries the next
		$results = pq('> div', $doc->find('#mainResults'));

		//If emtpy, tries a method
		if (empty($results->text()))
		{
			// METHOD 1 ---------------- There are two blocks to crawl in
			$results1 = pq('ul > li', $doc->find('#atfResults'));
			$results2 = pq('ul > li', $doc->find('#btfResults'));
			//Extract results and merge them into a single array
			$categoryPageLinks1 = $this->extractLinksFromResultsATF($results1);
			$categoryPageLinks2 = $this->extractLinksFromResultsATF($results2);
			$categoryPageLinks = array_merge($categoryPageLinks1, $categoryPageLinks2);
		}
		else 
		{
			// METHOD 2 ----------------
			$results = pq('> div', $doc->find('#mainResults'));
			$categoryPageLinks = $this->extractLinksFromResultsMAIN($results);
		}

		//Ads the next page link
		//Get next page link (adding the host first)
		$categoryLinks['categoryPageLinks'] = $categoryPageLinks;
		//$categoryLinks['nextPageLink'] = 'http://' . $this->config['amazon_host'] . $doc->find('#pagnNextLink')->attr('href');
		$categoryLinks['nextPageLink'] = 'http://' . $this->config['amazon_host'] . pq('#pagn > .pagnCur + .pagnLink > a', $doc)->attr('href');
		$categoryLinks['nextPageNumber'] = intval(pq('#pagn > .pagnCur', $doc)->text())+1;

		//Return
		return ($categoryLinks);
	}

	/**
	 * Given a div with book results, extracts 
	 *   each link and returns them into an array
	 * @param array $results 
	 * @return array
	 */
	private function extractLinksFromResultsMAIN($results)
	{
		//Declare array
		$categoryPageLinks = array();

		//Loop through each book's link to crawl it
		foreach ($results as $resultChild) 
		{
			//Extracts the book link from the child list
			$bookLink = pq('.image.imageContainer > a', $resultChild)->attr('href');
			$bookTitle = pq('.newaps > a > .lrg.bold', $resultChild)->text();

			$bookLinkData['bookLink'] = $bookLink;
			$bookLinkData['bookTitle'] = md5($bookTitle);

			//Add it to the array
			array_push($categoryPageLinks, $bookLinkData);
		}

		return ($categoryPageLinks);
	}

	/**
	 * This function belongs to the first extraction method
	 *  of the link's crawler. Extract the book's link and title 
	 *  directly from the html.
	 * @param array $results 
	 * @return array
	 */
	private function extractLinksFromResultsATF($results)
	{
		//Declare array
		$categoryPageLinks = array();

		//Loop through each book's link to crawl it
		foreach ($results as $resultChild) 
		{
			//Extracts the book link from the child list
			$bookLink = pq('.a-link-normal.s-access-detail-page.a-text-normal ', $resultChild)->attr('href');
			$bookTitle = pq('.a-link-normal.s-access-detail-page.a-text-normal ', $resultChild)->attr('title');

			$bookLinkData['bookLink'] = $bookLink;
			$bookLinkData['bookTitle'] = md5($bookTitle);

			//Add it to the array
			array_push($categoryPageLinks, $bookLinkData);
		}

		return ($categoryPageLinks);
	}

	/**
	  * Test printing of the results
	  * @param array $results 
	  * @return void
	  */ 
	private function printLinkResults($results)
	{
		echo ('LINK LIST');
		echo ('</br>');

        //Print some info
        foreach ($results as $link) 
        {
            echo ("Link: " . substr($link['bookTitle'],0,50) . ",  " . substr($link['bookLink'],0,50) . "...");
            echo ('</br>');
        }
	}
}

?>