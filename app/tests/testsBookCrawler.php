<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

        //Include app
        include_once("app/AmazonLinkCrawler.php");
        include_once("app/AmazonBookCrawler.php");
        include_once("app/model/AmazonLinksModel.php");
        include_once("app/model/AmazonBooksModel.php");
        
        //--------------------------------------
        //--------- CONTROLLER (CRAWLING) ------
        //--------------------------------------
        //--------------------------------------
                
        //************Testing single book crawling
/*        $link = 'http://www.amazon.es/cirujano-Las-Indias-Jacinto-Rey-ebook/dp/B004KKZ4M0/ref=sr_1_20/275-3666228-6831247?s=books&ie=UTF8&qid=1414758105&sr=1-20';
        $linkID = 0;
        $categoryID = 0;

        $booksCrawler = new AmazonBookCrawler();
        $book = $booksCrawler->crawlBookPage($link, $linkID, $categoryID);

        echo ("startsript <br/>");
        var_dump($book);
        echo ("endsript <br/>");*/

/*        //************Testing bookBatch crawl from batchLinks
        $categoryID = 0; 
        $first = 0;
        $last = 5;
        //First retrieves the linkbatch
        $linksModel = new AmazonLinksModel();
        $linksModel->connect();
        $batchLinks = $linksModel->findBookLinkBatchByRange($categoryID, $first, $last);
        $linksModel->close();
        //Exec the function
        $booksCrawler = new AmazonBookCrawler();
        $books = $booksCrawler->crawlBookBatch($batchLinks, $categoryID);

        //Test printing
        foreach ($books as $book)
        {
            var_dump($book);
        }*/

        //************Testing resumeCrawling
        $categoryID = 0; 
        $maxBooks = 5;

        //Exec the function
        $booksCrawler = new AmazonBookCrawler();
        echo ("startsript <br/>");
        $booksCrawler->resumeCrawling($categoryID, $maxBooks);
        echo ("endsript <br/>");

        //--------------------------------------
        //-------------- MODEL -----------------
        //--------------------------------------
        //--------------------------------------

/*        //*******************Testing basics
        $booksModel = new AmazonBooksModel();
        $booksModel->connect();
        $booksModel->newStatus();
        $booksModel->close();
        //$linkstatus = $linksModel->lastStatus(3);
        //echo("Last status at: " . $linkstatus->crawlingDate);
        echo ("endsript");*/

        //********************Testing single book insertion
/*        $link = 'http://www.amazon.es/fuego-del-desierto-Karen-Winter-ebook/dp/B00INVKO5E/ref=lp_902675031_1_4?s=books&ie=UTF8&qid=1414780304&sr=1-4';
        $linkID = 0;
        $categoryID = 0;

        $booksCrawler = new AmazonBookCrawler();
        $book = $booksCrawler->crawlBookPage($link, $linkID, $categoryID);

        $booksModel = new AmazonBooksModel();
        $booksModel->connect();
        $booksModel->newBookFromValues($categoryID, $book['title'], $book['price'], $book['rating'], $book['authorID'], $book['linkID'], $book['isbn'], $book['nPages']);
        $booksModel->close();

        echo ("startsript");
        var_dump($book);
        echo ("endsript");*/

        ?>
    </body>
</html>
