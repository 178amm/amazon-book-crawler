<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

        //Include app
        include_once("app/AmazonLinkCrawler.php");
        include_once("app/AmazonBookCrawler.php");
        include_once("app/model/AmazonLinksModel.php");
        
        //--------------------------------------
        //--------- CONTROLLER (CRAWLING) ------
        //--------------------------------------
        //--------------------------------------
                
        //*************Testing crawlCategoryLinks
/*        $catURL = "http://www.amazon.es/s/ref=lp_599364031_nr_n_0?rh=n%3A599364031%2Cn%3A%21599365031%2Cn%3A902675031&bbn=599365031&ie=UTF8&qid=1414525879&rnid=599365031";
        $linksCrawler = new AmazonLinkCrawler();
        $categorylinks = $linksCrawler->crawlCategoryLinks($catURL, 1);

        echo ('</br>');
        echo ('</br>');
        echo ('</br>');
        echo ('</br>');
        echo ('</br>');
        foreach ($categorylinks as $link) 
        {
            echo ("Link: " . substr($link,0,50) . "...");
            echo ('</br>');
        }*/
        

        //***************Testing crawlCategoryPageLinks
/*        //pag1
        $catURL = "http://www.amazon.es/s/ref=sr_pg_1?rh=n%3A599364031%2Cn%3A%21599365031%2Cn%3A902595031&ie=UTF8&qid=1414520681&ajr=2";
        //pag2
        //$catURL = "http://www.amazon.es/s/ref=sr_pg_6?rh=n%3A599364031%2Cn%3A%21599365031%2Cn%3A902520031&page=6&ie=UTF8&qid=1414690066";
        $linksCrawler = new AmazonLinkCrawler();
        $categorylinks = $linksCrawler->crawlCategoryPageLinks($catURL, 0);

*/
/*
        //***************Testing crawlBookPage
        $catURL = "http://www.amazon.es/CAPIT%C3%81N-RILEY-novela-aventuras-del-ebook/dp/B00L4YW7L6/ref=lp_902675031_1_9?s=books&ie=UTF8&qid=1414489839&sr=1-9";
        $bookCrawler = new AmazonBookCrawler();
        $bookdata = $bookCrawler->crawlBookPage($catURL);
        //Print some info
        echo($bookdata['title']);
        echo($bookdata['price']);
        echo($bookdata['npages']);
        echo($bookdata['ISBN']);
        echo($bookdata['ctime'] . " sec");*/

        //--------------------------------------
        //-------------- MODEL -----------------
        //--------------------------------------
        //--------------------------------------

        //*************Testing basics
/*        $linksModel = new AmazonLinksModel();
        //$linksModel->newStatus();
        $linkstatus = $linksModel->lastStatus(3);
        echo("Last status at: " . $linkstatus->crawlingDate);
        echo ("endsript");*/

        //*************Testing AmazonLink status database
/*        //public function startCrawling($categoryURL, $categoryID, $maxPages)
        $categoryURL = 'http://www.amazon.es/s/ref=lp_599364031_nr_n_0?rh=n%3A599364031%2Cn%3A%21599365031%2Cn%3A902675031&bbn=599365031&ie=UTF8&qid=1414694267&rnid=599365031';
        $categoryID = 0;
        $maxPages = 3;

        $linksCrawler = new AmazonLinkCrawler();
        //$linksCrawler->startCrawling($categoryURL, $categoryID, $maxPages);
        $linksCrawler->resumeCrawling($categoryID, $maxPages);*/

        ///*************Testing batch link retrieving
        $categoryID = 0;
        $first = 0;
        $last = 2;

        $linksModel = new AmazonLinksModel();
        $linksModel->connect();
        $batch = $linksModel->findBookLinkBatchByRange($categoryID, $first, $last);
        $linksModel->close();

        echo ("startsript");
        echo ('</br>');

        foreach ($batch as $bookLink)
        {
            echo ("LinkID: " . $bookLink['id'] . ", Link: " . substr($bookLink['link'],0,50) . "...");
            echo ('</br>');
        }

        echo ("endsript");
        echo ('</br>');

        ?>
    </body>
</html>
