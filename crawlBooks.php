<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php     

    //Include from app
    include_once("app/AmazonLinkCrawler.php");
    include_once("app/AmazonBookCrawler.php");
    include_once("app/model/AmazonLinksModel.php");
    include_once("app/model/AmazonBooksModel.php");


    function resume()
    {
        //Continues crawling 5 books from each category since the last crawling
        $maxBooks = 5;
        //Accion y aventura
        $categoryID0 = 0;
        //Arte, cine y fotografia
        $categoryID1 = 1;
        //Biografias, diarios y hechos reales
        $categoryID2 = 2;

        //Start crawling
        $booksCrawler = new AmazonBookCrawler();
        $booksCrawler->resumeCrawling($categoryID0, $maxBooks);
        $booksCrawler->resumeCrawling($categoryID1, $maxBooks);
        $booksCrawler->resumeCrawling($categoryID2, $maxBooks);
    }

?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1> Books crawler page </h1>
    </br>
        <a href="?run=resume">Resume</a>
    </br>
        <a href="./index.html">Back..</a>
    </br>
        <?php
        if (isset($_GET['run']))
        {
            echo ("<p> Crawling, it may take a while... </>");
            switch ($_GET['run']) {
                case 'resume':
                    resume();
                    break;
                default:
                    echo ("eror");
                    break;
            }
        }
        ?>
    
    </body>
</hmtl>